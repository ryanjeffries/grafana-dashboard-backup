backup-grafana() {
  backup-grafana-datasources
  backup-grafana-users
  backup-grafana-orgs
  backup-grafana-dashboards
}

restore-grafana() {
  restore-grafana-datasources
  restore-grafana-orgs
#  restore-grafana-users
}

backup-grafana-datasources() {
  curl -u "${GRAFANA_AUTH}" -X GET -k -sL "${GRAFANA_BACKUP_URL}/api/datasources" | jq '[.[] | del(.id) | del(.orgId)]' > datasources.json
}

# backup-grafana-users() {
#   curl -u "${GRAFANA_AUTH}" -X GET -k -sL "${GRAFANA_BACKUP_URL}/api/users" | jq '[.[]]' > users.json
# }

backup-grafana-orgs() {
  curl -u "${GRAFANA_AUTH}" -X GET -k -sL "${GRAFANA_BACKUP_URL}/api/orgs" | jq '[.[] | del(.id) | del(.orgId)]' > orgs.json
}

# backup-grafana-org-users() {
#   curl -u "${GRAFANA_AUTH}" -X GET -k -sL "${GRAFANA_BACKUP_URL}/api/org/users" | jq '[.[]]' | jq --arg [.[login]] bar '[.[] + {LoginOrEmail: $login}]' > orgUsers.json
# }

restore-grafana-orgs() {
  cat orgs.json | jq -c '.[]' | while read source; do
    echo "pushing $(echo $source | jq -r '.name'):"
    echo "$source" | curl -u "${GRAFANA_AUTH}" -sL -XPOST -H "Content-Type: application/json" "${GRAFANA_RESTORE_URL}/api/orgs" -d@-
    echo
  done
}

restore-grafana-org-users() {
  cat orgUsers.json | jq -c '.[]' | while read source; do
    echo "pushing $(echo $source | jq -r '.name'):"
    echo "$source" | curl -u "${GRAFANA_AUTH}" -sL -XPOST -H "Content-Type: application/json" "${GRAFANA_RESTORE_URL}/api/org/users" -d@-
    echo
  done
}

restore-grafana-datasources() {
  cat datasources.json | jq -c '.[]' | while read source; do
    echo "pushing $(echo $source | jq -r '.name'):"
    echo "$source" | curl -u "${GRAFANA_AUTH}" -sL -XPOST -H "Content-Type: application/json" "${GRAFANA_RESTORE_URL}/api/datasources" -d@-
    echo
  done
}

backup-grafana-dashboards() {
  mkdir -p dashboards

  for dash in $(curl -u "${GRAFANA_AUTH}" -X GET -k ${GRAFANA_BACKUP_URL}/api/search\?query\=\& |tr ']' '\n' |cut -d "," -f 5 | grep db |cut -d\" -f 4 |cut -d\/ -f2); do
    curl -X GET -u "${GRAFANA_AUTH}" -k ${GRAFANA_BACKUP_URL}/api/dashboards/db/$dash | sed 's/"id":[0-9]\+,/"id":null,/' | sed 's/\(.*\)}/\1,"overwrite": true}/' > dashboards/$dash.json
  done
}
